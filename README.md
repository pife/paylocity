# Paylocity

This is my coding challege presented to Paylocity.

In the API directory, there is a .Net Core Web Api to simulate the communication from the client and server.  It has a mocked employee that will be returned to the client.

- There is only one controller with a GET and a POST method that call down 2 seperate paths.
- There is a business layer that would handle all the translations and any business logic that may need to be implemented on the server side.
- The data layer is where we would save the information to the DB.
- This is a very slim implementation with my decision to put the calculations on the client and pass the values to the API.

In the web directory is a VueJs application for the client ui portion

- There is only one view however there are instances of sections that could be moved into more reusable components.  For example, the area that shows the benefit calculation could be moved into its own component.  In a full fledged application, this to me could be reused.
- I did used Vue's Plugin's a bit.  Allowed me to move all of the benefit calculation its own area.  Since again, this may be used in a full scale application.

Areas of Improvement

- Unit testing (API application and Vue application)
- View Models on the API.
- Translations of the View Model to the DB Model
- Look at making some components to break up the size of the single page being presented on the client.
- The ability to update the employee added or pulled from the API project is not present.
- Data Validations are a must in any application and this one does not have any.
- There are no doubt others but these are the ones that stuck out to me.
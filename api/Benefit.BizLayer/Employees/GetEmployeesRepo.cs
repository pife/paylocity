﻿using Benefit.Data.Employees.GetEmployee;
using Benefit.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Benefit.Business.Employees
{
    public interface IGetEmployeesRepo
    {
        List<Employee> GetEmployeesData();
    }

    public class GetEmployeesRepo : IGetEmployeesRepo
    {
        private readonly IGetEmployees _getEmployees;
        public GetEmployeesRepo(IGetEmployees getEmployees)
        {
            _getEmployees = getEmployees;
        }

        public List<Employee> GetEmployeesData()
        {
            var retData = _getEmployees.GetEmployeesData();
            return retData;
        }
    }
}

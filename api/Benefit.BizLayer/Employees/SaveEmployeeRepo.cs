﻿using Benefit.Data.Employees.SaveEmployee;
using Benefit.Models;
using System;

namespace Benefit.Business.Employees
{
    public interface ISaveEmployeeRepo
    {
        Employee SaveEmployeeData(Employee employee);
    }

    public class SaveEmployeeRepo : ISaveEmployeeRepo
    {
        private readonly ISaveEmployee _saveEmployee;

        public SaveEmployeeRepo(ISaveEmployee saveEmployee)
        {
            _saveEmployee = saveEmployee;
        }

        public Employee SaveEmployeeData(Employee employee)
        {
            // At this point I would look at the object being passed into here and send it into a translator.  Usually in my experience, the model coming from the
            // client will be a bit different.  This would also allow me to do data validations and translate it to the object needed to save to the DB.

            // The code below I would add to that translator.  My new employees are lacking an ID on it, this is my signal that a new employee is coming in.  This could
            // also be skipped and on insert to the table, have SQL Server generate the GUID.
            if (employee.Id == null || employee.Id == Guid.Empty)
            {
                var newEmployeeGuid = Guid.NewGuid();
                employee.Id = newEmployeeGuid;

                if (employee.Dependents.Count > 0)
                {
                    foreach (var empDependent in employee.Dependents)
                    {
                        empDependent.Id = Guid.NewGuid();
                        empDependent.EmployeeId = newEmployeeGuid;
                    }
                }
            }

            var dbEmployee = _saveEmployee.SaveEmployeeData(employee);
            return dbEmployee;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Benefit.Models;
using Benefit.Common;
using Benefit.Business.Employees;

namespace BenefitsApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BenefitController : ControllerBase
    {
        private readonly IGetEmployeesRepo _getEmployeesRepo;
        private readonly ISaveEmployeeRepo _saveEmployeeRepo;

        public BenefitController(IGetEmployeesRepo getEmployeesRepo, ISaveEmployeeRepo saveEmployeeRepo)
        {
            _getEmployeesRepo = getEmployeesRepo;
            _saveEmployeeRepo = saveEmployeeRepo;
        }

        [HttpGet]
        public List<Employee> Get()
        {
            var retData = _getEmployeesRepo.GetEmployeesData();
            return retData;
        }

        [HttpPost]
        public Employee AddOrUpdate(Employee employee)
        {
            var retData = _saveEmployeeRepo.SaveEmployeeData(employee);
            return retData;
        }
    }
}

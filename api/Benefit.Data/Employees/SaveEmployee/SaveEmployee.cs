﻿using Benefit.Models;

namespace Benefit.Data.Employees.SaveEmployee
{
    public interface ISaveEmployee
    {
        Employee SaveEmployeeData(Employee employee);
    }
    
    public class SaveEmployee : ISaveEmployee
    {
        public Employee SaveEmployeeData(Employee employee)
        {
            // This is the point where the employee would be saved to the database.  Just returning the employee passed in for this exercise.
            return employee;
        }
    }
}

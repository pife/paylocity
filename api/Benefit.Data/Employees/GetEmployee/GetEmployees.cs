﻿using Benefit.Common;
using Benefit.Models;
using System;
using System.Collections.Generic;

namespace Benefit.Data.Employees.GetEmployee
{
    public interface IGetEmployees
    {
        List<Employee> GetEmployeesData();
    }

    public class GetEmployees : IGetEmployees
    {
        public List<Employee> GetEmployeesData()
        {
            // Ideally this information would be coming from a DB.
            var empId = Guid.NewGuid();
            var emp = new Employee
            {
                Id = empId,
                FirstName = "Brian",
                LastName = "Pifer"
            };
            emp.Dependents.Add(
              new Dependent
              {
                  Id = Guid.NewGuid(),
                  FirstName = "Sarah",
                  LastName = "Pifer",
                  DepType = DependentType.Spouse,
                  EmployeeId = empId
              }
            );
            emp.Dependents.Add(
              new Dependent
              {
                  Id = Guid.NewGuid(),
                  FirstName = "Jacob",
                  LastName = "Pifer",
                  DepType = DependentType.Child,
                  EmployeeId = empId
              }
            );
            emp.Dependents.Add(
              new Dependent
              {
                  Id = Guid.NewGuid(),
                  FirstName = "Rachael",
                  LastName = "Pifer",
                  DepType = DependentType.Child,
                  EmployeeId = empId
              }
            );
            emp.Dependents.Add(
              new Dependent
              {
                  Id = Guid.NewGuid(),
                  FirstName = "Abram",
                  LastName = "Pifer",
                  DepType = DependentType.Child,
                  EmployeeId = empId
              }
            );

            return new List<Employee> { emp };
        }
    }
}

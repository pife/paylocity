﻿namespace Benefit.Common
{
    public enum DependentType
    {
        Child,
        Spouse
    }
}

﻿namespace Benefit.Models
{
    public class Paycheck
    {
        public decimal TotalCost { get; set; }
        public decimal TotalEmployeeCost { get; set; }
        public decimal TotalCostOfDependents { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal BasePaycheck { get; set; }
        public decimal BaseYearlySalary { get; set; }
        public decimal YearlySalary { get; set; }
    }
}

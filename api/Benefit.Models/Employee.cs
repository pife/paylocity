﻿using System.Collections.Generic;

namespace Benefit.Models
{
    public class Employee : Human
    {
        public Employee ()
        {
            Dependents = new List<Dependent>();
        }

        public Paycheck Paycheck { get; set; }

        public List<Dependent> Dependents { get; set; }
    }
}

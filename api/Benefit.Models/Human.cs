﻿using System;

namespace Benefit.Models
{
    public class Human
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

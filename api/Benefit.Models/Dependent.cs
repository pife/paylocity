﻿using Benefit.Common;
using System;

namespace Benefit.Models
{
    public class Dependent : Human
    {
        public Guid EmployeeId { get; set; }
        public DependentType DepType { get; set; }
    }
}

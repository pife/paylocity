function install(Vue) {
  Vue.CalculateBenefit = function (employee) {
    const baseCost = 1000.00;
    const baseCostOfDependent = 500.00;   
    const discountPercent = 0.10;
    const paycheckAmount = 2000.00;
    const paychecksPerYear = 26;

    let totalCost = 0.0;
    let totalEmployeeCost = 0.0;
    let totalCostOfDependents = 0.0;
    let totalDiscount = 0.0;

    if (checkNameForDiscount(employee.firstName)) {
      let totalOffEmployee = baseCost * discountPercent;
      totalDiscount += totalOffEmployee;
      totalEmployeeCost += baseCost - totalOffEmployee;
    } else {
      totalEmployeeCost += baseCost;
    }  

    employee.dependents.forEach(dep => {
      if (checkNameForDiscount(dep.firstName)) {
        let totalOffDependent = baseCostOfDependent * discountPercent;
        totalDiscount += totalOffDependent;
        totalCostOfDependents += baseCostOfDependent - totalOffDependent;
      } else {
        totalCostOfDependents += baseCostOfDependent;
      }      
    });

    totalCost = totalEmployeeCost + totalCostOfDependents;

    return {
      totalCost: totalCost.toFixed(2),
      totalEmployeeCost: totalEmployeeCost.toFixed(2),
      totalCostOfDependents: totalCostOfDependents.toFixed(2),
      totalDiscount: totalDiscount.toFixed(2),
      basePaycheck: paycheckAmount.toFixed(2),
      baseYearlySalary: (paycheckAmount * paychecksPerYear).toFixed(2),
      yearlySalary: ((paycheckAmount * paychecksPerYear) - totalCost).toFixed(2)
    }
  };

  let checkNameForDiscount = function (employeeName)
  {
    let firstLetter = employeeName.charAt(0);
    if (firstLetter.toLowerCase() === 'a') {
      return true;
    } else {
      return false;
    }
  }
}

export default install;
function install(Vue) {
  Vue.CloneObject = function (data) {
    return JSON.parse(JSON.stringify(data));
  };
}

export default install;